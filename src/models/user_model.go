package models

import (
	"database/sql"
	"entities"
)

type UserModel struct {
	Db *sql.DB
}

func (userModel UserModel) FindAll() (user []entities.User, err error) {

	rows, err := userModel.Db.Query("select * from user")
	if err != nil {
		return nil, err
	} else {
		var users []entities.User
		for rows.Next() {
			var id int64
			var name string
			var address string
			var number int64
			var pincode int64
			err2 := rows.Scan(&id, &name, &address, &number, &pincode)
			if err2 != nil {
				return nil, err2
			} else {
				user := entities.User{
					Id:      id,
					Name:    name,
					Address: address,
					Number:  number,
					Pincode: pincode,
				}
				users = append(users, user)
			}

		}
		return users, nil
	}
}

func (userModel UserModel) Create(user *entities.User) (err error) {
	result, err := userModel.Db.Exec("insert into user(name,address,number,pincode) values(?, ?, ?, ?)", user.Name, user.Address, user.Number, user.Pincode)
	if err != nil {
		return err
	} else {
		user.Id, _ = result.LastInsertId()
		return nil
	}

}

func (userModel UserModel) Update(user *entities.User) (int64, error) {
	result, err := userModel.Db.Exec("update user set name = ?, address = ?, number = ? , pincode=? where id = ?", user.Name, user.Address, user.Number, user.Pincode, user.Id)
	if err != nil {
		return 0, err
	} else {

		return result.RowsAffected()
	}

}

func (userModel UserModel) Delete(id int64) (int64, error) {
	result, err := userModel.Db.Exec("delete from user  where id =?", id)
	if err != nil {
		return 0, err
	} else {

		return result.RowsAffected()
	}

}
