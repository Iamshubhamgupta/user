package entities

import "fmt"

type User struct {
	Id      int64  `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Number  int64  `json:"number"`
	Pincode int64  `json:"pincode"`
}

func (user User) ToString() string {

	return fmt.Sprintf("id: %d\nname: %s\naddress: %s\nnumber: %d\npincode: %d", user.Id, user.Name, user.Address, user.Number, user.Pincode)
}
